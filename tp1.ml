#use "topfind";;
#require "qcheck";;
open QCheck;;
open Test;;
(**
    a,b>=0
*)
let rec aux_div a b i =
let s = a-b in
if s < 0 then (i,a)
else aux_div s b (i+1)
;;

(**

*)
exception Diviseur_nul

let div a b = if b=0 then raise Diviseur_nul else aux_div (abs a) (abs b) 0;;

let prop_div (a,b) = try
let q,r = div a b in
(r>=0) && (r<b) && ((abs a) = (abs b)*q + r)
with
|Diviseur_nul -> b=0
;;

let test_div = Test.make ~count:1000 (pair small_int small_int) prop_div;;

QCheck_runner.run_tests [test_div];;

(*Q2*)

(**)
let rec sum l = match l with
|[]->0
|a::l2 -> a+(sum l2)
;;

let prop_sum xs,ys = sum (xs@ys)= (sum xs) + (sum ys);;

let test_sum = Test.make ~count:1000 (pair (list small_int) (list small_int)) prop_sum;;

QCheck_runner.run_tests [test_sum];;

(*Q3*)
let rec merge l1 l2 = match l1,l2 with
|[],[] -> []
|[],a::l2' -> a::(merge l1 l2')
|a::l1',[] -> a::(merge l1' l2)
|a::l1',b::l2' -> if a<b then a::(merge l1' l2)
else b::(merge l1 l2')
;;

let prop_merge (xs,ys) =
List.sort compare (xs @ ys) = merge (List.sort compare xs) (List.sort compare ys)
;;

let test_merge = make ~count:1000 (pair (list small_int) (list small_int)) prop_merge;;

QCheck_runner.run_tests [test_merge];;

(*Q4*)

(* ----- FONCTIONS ----- *)


(**
    [fibo1 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    La méthode utilisée est la méthode naïve avec l'implémentation de la
    définition de F_n :
        F_n = F_(n-2) + F_(n-1)
      
    n est un entier, n>=0
*)
let rec fibo1 n = match n with
|0 -> 0
|1 -> 1
|_ -> fibo1 (n-2) + fibo1 (n-1)
;;


(**
    [fibo2 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    On utilise une fonction auxiliaire aux :

      [aux acc1 acc2] renvoie la valeur de acc1 lorsque n=0
      c'est une fonction récursive, à chaque passage dans aux on actualise 
      les accumulateurs :
        new_acc2 = acc1 + acc2
        new_acc1 = acc2
      et on baisse la valeur de n de 1

      Ainsi, à la fin, la valeur dans acc1 correspond à F_n 
      et la valeur dans acc2 à F_(n+1)
      
    On appelle la fonction aux avec les valeurs initiales de la suite de Fibonacci :
    [aux n 0 1] renvoie donc le résultat souhaité

    n est un entier, n>=0
    *)
let fibo2 n =
let rec aux n acc1 acc2 =
    if n = 0 then acc1
    else aux (n-1) acc2 (acc1 + acc2)
in
aux n 0 1
;;


(** [fibo3 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    On utilise les formules de récurrence selon la parité de n (fast doubling)
      F(2k) = F(k)*(2*F(k+1) - F(k))
      F(2k+1) = F(k)² + F(k+1)²
    
    n est un entier, n>=0
    *)
let rec fibo3 n =
    (*premiers termes*)
    match n with
    |0 -> 0
    |1 -> 1
    (* on ajoute le cas n=2 pour éviter de boucler lorsque k=1 *)
    |2 -> 1 
    (*n>2*)
    |_ ->
    let k = n/2 in
    let fk = fibo3 k and fkp1 = fibo3 (k+1)
    and r = n mod 2 in
    (* r est le reste de la division euclidienne de n par 2*)
    match r with
    |0 -> fk*(2*fkp1-fk) (* n=2k *)
    |1 -> fk*fk + fkp1*fkp1 (* n=2k+1 *)
    |_-> failwith "cas impossible"
;;


(* ----- PROPRIETES ----- *)

(* On considère les valeurs retournées par fibo1 comme valides *)
let prop_fibo2 n = fibo1 n = fibo2 n ;;

let prop_fibo3 n = fibo1 n = fibo3 n ;;


(* ----- TESTS ----- *)

(* Les temps de calculs de [fibo1 n] étant trop longs pour des valeurs
   de n > 40, on ajoute une condition dans les tests :
   [assume (n<=40)]
*)
let test_fibo2 = Test.make ~count:300 small_int
  (fun n -> assume (n<=40) ; prop_fibo2 n)
;;

let test_fibo3 = Test.make ~count:300 small_int 
  (fun n -> assume (n<=40) ; prop_fibo3 n)
;;

QCheck_runner.run_tests [test_fibo2];;
QCheck_runner.run_tests [test_fibo3];;



(* --------------------------- Q5 --------------------------- *)

type cmd =
| Add of string * int
| Remove of string
| Find of string
| Mem of string
;;

let gen_cmd =
    Gen.oneof
    [   Gen.map2 (fun k v -> Add (k,v)) Gen.small_string Gen.small_int;
        Gen.map (fun k -> Remove k) Gen.small_string;
        Gen.map (fun k -> Find k) Gen.small_string;
        Gen.map (fun k -> Mem k) Gen.small_string; ]
;;

let arb_cmd = QCheck.make gen_cmd ;;
let arb_cmds = list arb_cmd ;;

type state = (string * int) list ;;

type value = B of bool | I of int | Absent ;;

let rec mem_aux str lst = match lst with 
|[] -> false 
|(str2,_)::lst2 -> if String.equal str str2 then true
else mem_aux str lst2
;;

exception Not_found

let rec find_aux str lst = match lst with 
|[] -> raise Not_found 
|(str2,i)::lst2 -> if String.equal str str2 then i
else find_aux str lst2
;;

let rec remove_aux str lst = match lst with 
|[] -> lst
|(str2,i)::lst2 -> if String.equal str str2
  then lst2
  else (str2,i)::(remove_aux str lst2)
;;

let rec cmd_model c s = match c with 
(* pour add on ajoute la nouvelle association en début de liste
   si une association est déjà existante pour cette clé, on la 
   cache jusqu'à que la nouvelle association soit removed *)
|Add (str,i) -> ((str,i)::s),None
|Mem str -> (s,Some(B(mem_aux str s)))
|Find str -> (try (s,Some(I(find_aux str s)))
            with Not_found -> (s,Some(Absent)))
|Remove str -> ((remove_aux str s),None)
;;


let cmd_sut_correct c h = match c with 
|Add (str,i) -> Hashtbl.add h str i ; None
|Mem str -> Some(B(Hashtbl.mem h str))
|Find str -> (try Some(I(Hashtbl.find h str))
            with Not_found -> Some(Absent))
|Remove str -> Hashtbl.remove h str; None
;;

(*avec une erreur dans le add*)
let cmd_sut c h = match c with 
|Add (str,i) -> 
  (*nouvelle définition (erronée) du Hashtbl.add*)
  let new_Hashtbl_add h k v =
  if String.length k <= 2 then Hashtbl.add h k v
  else Hashtbl.add h k (v+1) in
  new_Hashtbl_add h str i ; None
|Mem str -> Some(B(Hashtbl.mem h str))
|Find str -> (try Some(I(Hashtbl.find h str))
            with Not_found -> Some(Absent))
|Remove str -> Hashtbl.remove h str; None
;;  

exception Empty_list
let rec interp_agree s h cs = match cs with 
|[] -> raise Empty_list
|[c] -> let _,res_model = cmd_model c s 
        in res_model = cmd_sut c h
|c::cs2 -> let new_s,res_model = cmd_model c s in
  if res_model = cmd_sut c h
  then interp_agree new_s h cs2
  else false
;;  

let test_hash = Test.make ~count:500 (arb_cmds) 
(fun cs -> try interp_agree [] (Hashtbl.create ~random:false 42) cs
      with Empty_list -> cs=[]) ;;

QCheck_runner.run_tests [test_hash];;

(*Ce n'est pas étonnant que les tests ne détectent pas l'erreur
   dans l'interprétation du Add, puisque pour la détecter il faudrait
   que la commande Find(key) soit appelé avec exactement la même
   chaîne de caractères, ce qui est peu probable. 
*)



(* --------------------------- Q6 --------------------------- *)

#use "topfind";;
#require "qcheck";;
open QCheck;;


type cmd =
| Push of int
| Pop
| Top
;;

let gen_cmd =
    Gen.oneof
    [   Gen.map (fun k -> Push k) Gen.small_int;
        Gen.return Pop;
        Gen.return Top; ]
;;

let arb_cmd = QCheck.make gen_cmd;;

(*La commande Create n'apparaît qu'une seule fois, au début*)
let arb_cmds = list arb_cmd;;

(*représentation de la file en liste d'entier*)
type state = int list ;;

(*La valeur est "Empty" lorsque l'erreur Queue.Empty est levée
   par Queue.top ou Queue.pop
  La valeur est I(i) lorsque Queue.top ou Queue.pop renvoie i*)
type value = I of int | Empty ;;



(* [push_aux i q] retourne la liste q où l'entier i ajouté en fin 
de liste *)
let rec push_aux i q = match q with 
|[] -> [i]
|a::q2 -> a::(push_aux i q2)
;;   

(* [top_aux q] renvoie l'élément au sommet de la liste q *)
let top_aux q = match q with 
|[] -> raise Queue.Empty
|t::q -> t
;;

(* [pop_aux q] renvoie l'élément t au sommet de la liste q
   ainsi que la liste q2 étant la liste q sans son sommet *)
let pop_aux q = match q with
|[] -> raise Queue.Empty
|t::q2 -> t,q2
;;

let rec cmd_model c q = match c with
|Push i -> (push_aux i q),None 
|Top -> (try (q,Some(I(top_aux q)))
    with Queue.Empty -> (q,Some(Empty)) )
|Pop -> (try let (t,new_q)=pop_aux q in
                  (new_q,Some(I(t)))
    with Queue.Empty -> (q,Some(Empty)) )
;;


let cmd_sut c q = match c with 
|Push i -> Queue.push i q ; None
|Top -> (try Some(I(Queue.top q))
        with Queue.Empty -> Some(Empty))
|Pop -> (try Some(I(Queue.pop q))
      with Queue.Empty -> Some(Empty))
;; 

exception Empty_list
let rec interp_agree q_model q cs = match cs with 
|[] -> raise Empty_list
|[c] -> let _,res_model = cmd_model c q_model
        in res_model = cmd_sut c q
|c::cs2 -> let new_q_model,res_model = cmd_model c q_model in
  if res_model = cmd_sut c q
  then interp_agree new_q_model q cs2
  else false
;; 

let test_queue = Test.make ~count:1000 arb_cmds 
(fun cs -> assume (cs <> []) ; 
  let q = Queue.create () in 
  interp_agree [] q cs
)      
;;

QCheck_runner.run_tests [test_queue];;
