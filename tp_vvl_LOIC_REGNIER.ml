#use "topfind";;
#require "qcheck";;
open QCheck;;

(* 
   -----------------------------------------------------------------------
   |                            EXERCICE 4                               |
   -----------------------------------------------------------------------
*)

(* ----- FONCTIONS ----- *)


(**
    [fibo1 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    La méthode utilisée est la méthode naïve avec l'implémentation de la
    définition de F_n :
        F_n = F_(n-2) + F_(n-1)
      
    n est un entier, n>=0
*)
let rec fibo1 n = match n with
|0 -> 0
|1 -> 1
|_ -> fibo1 (n-2) + fibo1 (n-1)
;;


(**
    [fibo2 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    On utilise une fonction auxiliaire aux :

      [aux acc1 acc2] renvoie la valeur de acc1 lorsque n=0
      c'est une fonction récursive, à chaque passage dans aux on actualise 
      les accumulateurs :
        new_acc2 = acc1 + acc2
        new_acc1 = acc2
      et on baisse la valeur de n de 1

      Ainsi, à la fin, la valeur dans acc1 correspond à F_n 
      et la valeur dans acc2 à F_(n+1)
      
    On appelle la fonction aux avec les valeurs initiales de la suite de Fibonacci :
    [aux n 0 1] renvoie donc le résultat souhaité

    n est un entier, n>=0
    *)
let fibo2 n =
let rec aux n acc1 acc2 =
    if n = 0 then acc1
    else aux (n-1) acc2 (acc1 + acc2)
in
aux n 0 1
;;


(** [fibo3 n] renvoie le nème terme de la suite de Fibonacci (= F_n)
    On utilise les formules de récurrence selon la parité de n (fast doubling)
      F(2k) = F(k)*(2*F(k+1) - F(k))
      F(2k+1) = F(k)² + F(k+1)²
    
    n est un entier, n>=0
    *)
let rec fibo3 n =
    (*premiers termes*)
    match n with
    |0 -> 0
    |1 -> 1
    (* on ajoute le cas n=2 pour éviter de boucler lorsque k=1 *)
    |2 -> 1 
    |_ -> (*n>2*)
    let k = n/2 in
    let fk = fibo3 k and fkp1 = fibo3 (k+1)
    and r = n mod 2 in
    (* r est le reste de la division euclidienne de n par 2*)
    match r with
    |0 -> fk*(2*fkp1-fk) (* n=2k *)
    |1 -> fk*fk + fkp1*fkp1 (* n=2k+1 *)
    |_-> failwith "cas impossible"
;;


(* ----- PROPRIETES ----- *)

(* On considère les valeurs retournées par fibo1 comme références *)
let prop_fibo2 n = fibo1 n = fibo2 n ;;

let prop_fibo3 n = fibo1 n = fibo3 n ;;


(* ----- TESTS ----- *)

(* Les temps de calculs de [fibo1 n] étant trop longs pour des valeurs
   de n > 40, on ajoute une condition dans les tests :
   [assume (n<=40)]
*)
let test_fibo2 = Test.make ~count:200 small_int
  (fun n -> assume (n<=40) ; prop_fibo2 n)
;;

let test_fibo3 = Test.make ~count:200 small_int 
  (fun n -> assume (n<=40) ; prop_fibo3 n)
;;

QCheck_runner.run_tests [test_fibo2];;
QCheck_runner.run_tests [test_fibo3];;


(* Pour tester des valeurs supérieures à n=40
   on considère fibo2 comme référence*)
let prop_fibo3_2 n = fibo2 n = fibo3 n ;;

let test_fibo3_2 = Test.make ~count:1000 small_int 
(fun n -> prop_fibo3_2 n)
;;

QCheck_runner.run_tests [test_fibo3_2];;

(* 
   -----------------------------------------------------------------------
   |                            EXERCICE 6                               |
   -----------------------------------------------------------------------
*)

(* Type regroupant les commandes à tester *)
type cmd =
| Push of int
| Pop
| Top
;;

(* Générationn d'une commande parmi celles à tester *)
let gen_cmd =
    Gen.oneof
    [   Gen.map (fun k -> Push k) Gen.small_int;
        Gen.return Pop;
        Gen.return Top; ]
;;


let arb_cmd = QCheck.make gen_cmd;;

let arb_cmds = list arb_cmd;;

(*Représentation de la file en liste d'entier*)
type state = int list ;;

(*La valeur est "Empty" lorsque l'erreur Queue.Empty est levée
   par Queue.top ou Queue.pop
  La valeur est I(i) lorsque Queue.top ou Queue.pop renvoie i*)
type value = I of int | Empty ;;



(* [push_aux i q] retourne la liste q où l'entier i ajouté en fin 
de liste *)
let rec push_aux i q = match q with 
|[] -> [i]
|a::q2 -> a::(push_aux i q2)
;;   

(* [top_aux q] renvoie l'élément au sommet de la liste q *)
let top_aux q = match q with 
|[] -> raise Queue.Empty
|t::q -> t
;;

(* [pop_aux q] renvoie le couple (t,q2) :
  t est le sommet de la liste q
  q2 est la liste q sans son sommet t *)
let pop_aux q = match q with
|[] -> raise Queue.Empty
|t::q2 -> t,q2
;;


(* [cmd_model c q] fournit l’état (type state) après l’exécution de la
  commande c sur la file q, ainsi que l'éventuelle valeur retournée
  (None s’il n’y en a pas) *)
let rec cmd_model c q = match c with
|Push i -> (push_aux i q),None 
|Top -> (try (q,Some(I(top_aux q)))
    with Queue.Empty -> (q,Some(Empty)) )
|Pop -> (try let (t,new_q)=pop_aux q in
                  (new_q,Some(I(t)))
    with Queue.Empty -> (q,Some(Empty)) )
;;


(* [cmd_sut c q] fournit l'éventuelle valeur retournée
  après l’exécution de la commande c sur la file q
  (None s’il n’y en a pas) *)
let cmd_sut c q = match c with 
|Push i -> Queue.push i q ; None
|Top -> (try Some(I(Queue.top q))
        with Queue.Empty -> Some(Empty))
|Pop -> (try Some(I(Queue.pop q))
      with Queue.Empty -> Some(Empty))
;; 


exception Empty_list

(* [interp_agree q_model q cs] compare les éventuelles
   valeurs retournées après l'exécution de toutes les 
   commandes de la liste cs sur la file q *)
let rec interp_agree q_model q cs = match cs with 
|[] -> raise Empty_list
|[c] -> let _,res_model = cmd_model c q_model
        in res_model = cmd_sut c q
|c::cs2 -> let new_q_model,res_model = cmd_model c q_model in
  (res_model = cmd_sut c q) && (interp_agree new_q_model q cs2)
;; 


(* On teste la fonction précédente avec une liste de
   commandes générée par qcheck 
   On ne traite pas le cas où la liste générée est vide *)
let test_queue = Test.make ~count:1000 arb_cmds 
(fun cs -> assume (cs <> []) ; 
  let q = Queue.create () in 
  interp_agree [] q cs
)      
;;

QCheck_runner.run_tests [test_queue];;
